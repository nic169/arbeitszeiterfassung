﻿using Arbeitszeiterfassung.Commands;
using Arbeitszeiterfassung.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Arbeitszeiterfassung.ViewModels
{
    public class MainWindowViewModel : BaseViewModel
    {
        public INavigationService NavService { get; set; } = NavigationService.Instance();

        public MainWindowViewModel()
        {
            this.NavService.SelectedViewModel = new LoginViewModel();
        }
    }
}
