﻿using Arbeitszeiterfassung.Commands;
using Arbeitszeiterfassung.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Arbeitszeiterfassung.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        public string Username { get; set; }

        public INavigationService NavService { get; set; } = NavigationService.Instance();

        public ICommand ValidateLoginCommand
        {
            get
            {
                return new CommandHandlerParam((Param) => this.ValidateLogin(Param));
            }
        }

        public LoginViewModel()
        {
            var users = this.sqliteService.SelectUsers();

            if (users.Count == 0)
            {
                // Default user for testing purposes            
                this.sqliteService.InsertUser("Default", "User", 8, 24, "defUsr", "defPwd");
            }
        }

        private void ValidateLogin(object param)
        {
            var users = this.sqliteService.SelectUsers();
            var passwordBox = (PasswordBox)param;
            var password = passwordBox.Password;
            var id = 0;
            var workingHours = 0;
            bool validUsername = false;
            bool validPassword = false;           

            // Check if username exists
            foreach (var item in users)
            {
                if (item.Username == this.Username)
                {
                    validUsername = true;
                    break;
                }
            }

            if (!validUsername)
            {
                MessageBox.Show("Benutzername nicht vorhanden");
            }
            else
            {
                // Check if password is correct
                foreach (var item in users)
                {
                    if (item.Username == this.Username && item.Password == password)
                    {
                        validPassword = true;
                        id = item.Id;
                        workingHours = item.WorkingHours;
                        break;
                    }
                    else
                    {
                        MessageBox.Show("Falsches Passwort");
                    }
                }
            }

            // Credentials correct; load dashboard
            if (validUsername && validPassword)
            {
                Properties.Settings.Default.CurrentUser_Uname = this.Username;
                Properties.Settings.Default.CurrentUser_Id = id;
                Properties.Settings.Default.CurrentUser_WorkingHours = workingHours;
                
                LoadViewCommand loadViewCommand = new LoadViewCommand(this.NavService);
                loadViewCommand.Execute(ViewType.Dashboard);
            }
        }
    }
}
