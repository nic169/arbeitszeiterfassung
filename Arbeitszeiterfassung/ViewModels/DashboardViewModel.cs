﻿using Arbeitszeiterfassung.Models;
using Arbeitszeiterfassung.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arbeitszeiterfassung.ViewModels
{
    public class DashboardViewModel : BaseViewModel
    {
        public string Greeting { get; set; } = "Hallo, " + Properties.Settings.Default.CurrentUser_Uname + "!";

        public INavigationService NavService { get; set; } = NavigationService.Instance();

        private List<Workday> workdays;

        public List<Workday> Workdays
        {
            get
            {
                return this.workdays;
            }
            set
            {
                this.SetValue(ref this.workdays, value);
            }
        }


        public DashboardViewModel()
        {
            this.Workdays = this.sqliteService.SelectWorkdays(Properties.Settings.Default.CurrentUser_Id);   
        }
    }
}
