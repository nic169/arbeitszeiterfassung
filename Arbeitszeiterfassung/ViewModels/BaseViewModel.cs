﻿using Arbeitszeiterfassung.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Arbeitszeiterfassung.ViewModels
{
    public class BaseViewModel : INotifyPropertyChanged
    {
        public SQLiteService sqliteService = new SQLiteService();
        
        public string Background { get; private set; } = "#232323";
        
        public string TextColor { get; private set; } = "#9d9d9d";

        public string InputBackground { get; private set; } = "#191919";

        public string InputTextColor { get; private set; } = "White";

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        // Checks if the two parameters are equal and notifys the View if they're not        
        public void SetValue<T>(ref T Backingfield, T value, [CallerMemberName] string propertyName = null)
        {
            if (EqualityComparer<T>.Default.Equals(Backingfield, value))
            {
                return;
            }
            else
            {
                Backingfield = value;

                this.OnPropertyChanged(propertyName);
            }
        }
    }
}
