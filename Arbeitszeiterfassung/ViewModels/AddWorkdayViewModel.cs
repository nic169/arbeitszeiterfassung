﻿using Arbeitszeiterfassung.Commands;
using Arbeitszeiterfassung.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Arbeitszeiterfassung.ViewModels
{
    public class AddWorkdayViewModel : BaseViewModel
    {
        public string Begin { get; set; }

        public string End { get; set; }

        public string Breaks { get; set; }

        public string Notes { get; set; }

        public INavigationService NavService { get; set; } = NavigationService.Instance();

        public ICommand AddWorkdayCommand
        {
            get
            {
                return new CommandHandler(() => this.AddWorkday());
            }
        }

        private DateTime selectedDate = DateTime.Now;

        public DateTime SelectedDate
        {
            get
            {
                return this.selectedDate;
            }
            set
            {
                this.SetValue(ref this.selectedDate, value);
                this.DateString();
            }
        }

        private string showDate = "Datum auswählen =>";

        public string ShowDate
        {
            get
            {
                return this.showDate;
            }
            set
            {
                this.SetValue(ref this.showDate, value);
            }
        }

        private bool isVacation;

        public bool IsVacation
        {
            get
            {
                return this.isVacation;
            }
            set
            {
                this.SetValue(ref this.isVacation, value);

                if (value)
                {
                    this.SickEnabled = false;
                }
                else
                {
                    this.SickEnabled = true;
                }
            }
        }

        private bool isSick;

        public bool IsSick
        {
            get
            {
                return this.isSick;
            }
            set
            {
                this.SetValue(ref this.isSick, value);

                if (value)
                {
                    this.VacationEnabled = false;
                }
                else
                {
                    this.VacationEnabled = true;
                }
            }
        }

        private bool vacationEnabled;

        public bool VacationEnabled
        {
            get
            {
                return this.vacationEnabled;
            }
            set
            {
                this.SetValue(ref this.vacationEnabled, value);
            }
        }

        private bool sickEnabled;

        public bool SickEnabled
        {
            get
            {
                return this.sickEnabled;
            }
            set
            {
                this.SetValue(ref this.sickEnabled, value);
            }
        }

        public AddWorkdayViewModel()
        {
            this.VacationEnabled = true;
            this.SickEnabled = true;
            this.Notes = string.Empty;
        }

        private void DateString()
        {
            this.ShowDate = this.SelectedDate.ToString("dddd, dd.MM.yyyy");
        }

        private void AddWorkday()
        {
            var ckBegin = DateTime.TryParseExact(this.Begin, "HH:mm", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime begin);
            var ckEnd = DateTime.TryParseExact(this.End, "HH:mm", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime end);
            var ckBreaks = int.TryParse(this.Breaks, out int breaks);

            if (ckBegin && ckEnd && ckBreaks)
            {
                var id = Properties.Settings.Default.CurrentUser_Id;
                //var date = SelectedDate.ToString("dd.MM.yyyy");                
                var date = this.SelectedDate;
                var workingHours = end - begin;
                var workingHoursTotal = workingHours.Subtract(TimeSpan.FromMinutes(breaks));
                var workingHoursDb = Properties.Settings.Default.CurrentUser_WorkingHours;
                var difference = workingHoursTotal.Subtract(TimeSpan.FromHours(workingHoursDb));

                this.sqliteService.InsertWorkday(id, date, this.Begin, this.End, breaks, difference, this.Notes, this.IsVacation, this.IsSick, false);

                LoadViewCommand loadViewCommand = new LoadViewCommand(this.NavService);
                loadViewCommand.Execute(ViewType.Dashboard);
            }
            else
            {
                if (!ckBegin)
                {
                    MessageBox.Show("Ungültige Sartzeit");
                }
                else if (!ckEnd)
                {
                    MessageBox.Show("Ungültige Endzeit");
                }
                else
                {
                    MessageBox.Show("Ungültige Pausenzeit");
                }
            }
        }
    }
}
