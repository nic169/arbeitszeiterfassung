﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Arbeitszeiterfassung.Commands
{
    public class CommandHandlerParam : ICommand
    {
        public event EventHandler CanExecuteChanged;

        private readonly Action<object> action;

        public CommandHandlerParam(Action<object> action)
        {
            this.action = action;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            this.action(parameter);
        }
    }
}
