﻿using Arbeitszeiterfassung.Services;
using Arbeitszeiterfassung.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Arbeitszeiterfassung.Commands
{
    public class LoadViewCommand : ICommand
    {
        private readonly INavigationService navigationService;

        public event EventHandler CanExecuteChanged;

        public LoadViewCommand(INavigationService navigationService)
        {
            this.navigationService = navigationService;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            if (parameter is ViewType viewType)
            {
                switch (viewType)
                {
                    case ViewType.Login:
                        this.navigationService.SelectedViewModel = new LoginViewModel();
                        break;
                    case ViewType.Dashboard:
                        this.navigationService.SelectedViewModel = new DashboardViewModel();
                        break;
                    case ViewType.AddWorkday:
                        this.navigationService.SelectedViewModel = new AddWorkdayViewModel();
                        break;
                    case ViewType.Profile:
                        //this.navigationService.SelectedViewModel = new ProfileViewModel();
                        break;
                    case ViewType.NewProfile:
                        //this.navigationService.SelectedViewModel = new NewProfileViewModel();
                        break;
                    default:
                        break;
                }
            }
        }
    }
}