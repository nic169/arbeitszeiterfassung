﻿using Arbeitszeiterfassung.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Arbeitszeiterfassung.Views;
using Arbeitszeiterfassung.ViewModels;

namespace Arbeitszeiterfassung
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            this.InitializeComponent();            
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            SQLiteService sqlite = new SQLiteService();
            sqlite.InitializeDatabase();

            Window window = new MainWindow();                        
            window.Show();

            base.OnStartup(e);
        }
    }
}
