﻿using Arbeitszeiterfassung.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Arbeitszeiterfassung.Views
{
    public partial class AddWorkdayView : UserControl
    {
        private readonly AddWorkdayViewModel viewModel;
        
        public AddWorkdayView()
        {
            this.InitializeComponent();

            this.viewModel = new AddWorkdayViewModel();
            this.DataContext = viewModel;
        }
    }
}
