﻿using Arbeitszeiterfassung.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Arbeitszeiterfassung.Views
{
    public partial class DashboardView : UserControl
    {
        private readonly DashboardViewModel viewModel;

        public DashboardView()
        {
            this.InitializeComponent();

            this.viewModel = new DashboardViewModel();
            this.DataContext = this.viewModel;
        }
    }
}
