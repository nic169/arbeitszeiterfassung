﻿using Arbeitszeiterfassung.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Arbeitszeiterfassung.Services
{
    public enum ViewType
    {
        Login,
        Dashboard,
        AddWorkday,
        Profile,
        NewProfile
    }
    
    public interface INavigationService
    {
        BaseViewModel SelectedViewModel { get; set; }

        ICommand UpdateViewModelCommand { get; }
    }
}
