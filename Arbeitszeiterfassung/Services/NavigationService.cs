﻿using Arbeitszeiterfassung.Commands;
using Arbeitszeiterfassung.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Arbeitszeiterfassung.Services
{
    public class NavigationService : INavigationService, INotifyPropertyChanged
    {
        private readonly static INavigationService instance = new NavigationService();

        private NavigationService() { }

        public static INavigationService Instance()
        {
            return instance;
        }
        
        public ICommand UpdateViewModelCommand => new LoadViewCommand(this);

        public event PropertyChangedEventHandler PropertyChanged;


        private BaseViewModel selectedViewModel;

        public BaseViewModel SelectedViewModel
        {
            get
            {
                return this.selectedViewModel;
            }
            set
            {
                this.selectedViewModel = value;
                this.OnPropertyChanged(nameof(SelectedViewModel));

            }
        }
        
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
