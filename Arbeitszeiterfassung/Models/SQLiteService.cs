﻿using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arbeitszeiterfassung.Models
{
    public class SQLiteService
    {
        private readonly SQLiteAsyncConnection connection;

        public SQLiteService()
        {
            var database = "arbeitszeiterfassung.db";
            var folderPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            var dbPath = Path.Combine(folderPath, database);

            this.connection = new SQLiteAsyncConnection(dbPath);
        }

        public void InitializeDatabase()
        {

            this.connection.CreateTableAsync<User>();
            this.connection.CreateTableAsync<Workday>();
            this.connection.CloseAsync();
        }

        // Interact with User table
        public void InsertUser(string firstName, string lastName, int workingHours, int vacationDays, string username, string password)
        {
            User user = new User
            {
                FirstName = firstName,
                LastName = lastName,
                WorkingHours = workingHours,
                VacationDays = vacationDays,
                Username = username,
                Password = password
            };

            this.connection.CreateTableAsync<User>();
            this.connection.InsertAsync(user);
            this.connection.CloseAsync();
        }

        public List<User> SelectUsers()
        {
            List<User> users;
            this.connection.CreateTableAsync<User>().GetAwaiter().GetResult();
            users = this.connection.Table<User>().ToListAsync().GetAwaiter().GetResult();
            
            this.connection.CloseAsync();

            return users;
    }       

        // Interact with Workday table
        public void InsertWorkday(int id, DateTime date, string begin, string end, int breaks, TimeSpan difference, string notes, bool isVacation, bool isSick, bool isEditBlocked)
        {
            Workday workday = new Workday
            {
                UserId = id,
                Date = date,
                Begin = begin,
                End = end,
                Breaks = breaks,
                Difference = difference,
                Notes = notes,
                IsVacation = isVacation,
                IsSick = isSick,
                IsEditBlocked = isEditBlocked
            };

            this.connection.CreateTableAsync<Workday>();
            this.connection.InsertAsync(workday);            
            this.connection.CloseAsync();
        }

        public List<Workday> SelectWorkdays(int id)
        {
            List<Workday> workdays;
            this.connection.CreateTableAsync<Workday>();
            workdays = this.connection.QueryAsync<Workday>("SELECT * FROM Workday WHERE UserId = ?", id).GetAwaiter().GetResult();
            workdays.OrderBy(x => x.Date.Date);
            this.connection.CloseAsync();

            List<Workday> workdays2 = workdays.OrderByDescending(o => o.Date.Date).ToList();
            return workdays2;
        }

        public void Update()
        {

        }

        public void Delete()
        {

        }
    }
}
