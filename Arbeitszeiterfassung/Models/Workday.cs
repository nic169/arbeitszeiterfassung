﻿using SQLite;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arbeitszeiterfassung.Models
{
    public class Workday
    {
        public int UserId { get; set; }

        public DateTime Date { get; set; }

        public string Begin { get; set; }

        public string End { get; set; }

        public int Breaks { get; set; }

        public TimeSpan Difference { get; set; }

        public string Notes { get; set; }

        public bool IsVacation { get; set; }

        public bool IsSick { get; set; }

        public bool IsEditBlocked { get; set; }
    }
}
